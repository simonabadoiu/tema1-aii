DROP DATABASE erp;

CREATE DATABASE erp;

USE erp;

CREATE TABLE departamente (
	iddepartament	INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	denumire	VARCHAR(30) NOT NULL
);

CREATE TABLE functii (
	idfunctie	INT(10)	UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	iddepartament	INT(10) UNSIGNED NOT NULL,
	denumire	VARCHAR(30),
	descriere	VARCHAR(100),
	FOREIGN KEY (iddepartament) REFERENCES departamente(iddepartament) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE utilizatori (
    idutilizator        INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
    CNP                 BIGINT(13) NOT NULL,
    nume                VARCHAR(30) NOT NULL,
    prenume             VARCHAR(30) NOT NULL,
    adresa              VARCHAR(100) NOT NULL,
    telefon             INT(10),
    email               VARCHAR(60) NOT NULL,
    IBAN                VARCHAR(34) NOT NULL,
    nrcontract          INT(10) NOT NULL,
    dataangajarii       DATE NOT NULL,
    tip                 VARCHAR(30) NOT NULL DEFAULT 'angajat',
    zileconcediuramase	INT(10) NOT NULL DEFAULT 15,
    numarorecontract    INT(10) NOT NULL,     
    salariunegociat     INT(10) NOT NULL,
    numeutilizator      VARCHAR(30) NOT NULL,
    parola              VARCHAR(30) NOT NULL
);
ALTER TABLE utilizatori ADD CONSTRAINT email_chk CHECK (email LIKE '%@%.%');
ALTER TABLE utilizatori ADD CONSTRAINT tip_chk CHECK (tip IN ('administrator', 'angajat', 'super-administrator'));

CREATE TABLE asociereutilizatorfunctie (
	idasociere	INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	idutilizator	INT(10) UNSIGNED NOT NULL,
	idfunctie	INT(10) UNSIGNED NOT NULL,
	FOREIGN KEY (idutilizator) REFERENCES utilizatori(idutilizator) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (idfunctie) REFERENCES functii(idfunctie) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE asocieredepartamentresponsabil(
	idasociere	INT(10)	UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	iddepartament	INT(10) UNSIGNED NOT NULL,
	idresponsabil	INT(10) UNSIGNED NOT NULL,
	FOREIGN KEY (idresponsabil) REFERENCES utilizatori(idutilizator) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (iddepartament) REFERENCES departamente(iddepartament) ON UPDATE CASCADE ON DELETE CASCADE	
);

#CREATE TABLE detaliiangajati (
#	iddetaliu		        INT(10)	UNSIGNED AUTO_INCREMENT PRIMARY #KEY NOT NULL,	
#	zileconcediuodihna	    INT(10) NOT NULL DEFAULT 10,
#	startpauzademasa	    TIME NOT NULL,
#	terminarepauzademasa	TIME NOT NULL,
#	inceputprogram		    TIME NOT NULL,
#	terminareprogram	    TIME NOT NULL
#);

CREATE TABLE concedii (
	idconcediu	    INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	idutilizator	INT(10) UNSIGNED NOT NULL,
	datainceput	    DATETIME NOT NULL,
	durata		    INT(10) NOT NULL,
	tip		        VARCHAR(20) NOT NULL DEFAULT 'odihna',
	FOREIGN KEY (idutilizator) REFERENCES utilizatori(idutilizator) ON UPDATE CASCADE ON DELETE CASCADE
);
ALTER TABLE concedii ADD CONSTRAINT tip_chk CHECK (tip IN ('odihna', 'medical', 'fara plata', 'motive speciale'));

CREATE TABLE activitatezilnica (
	idactivitate	INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	idutilizator	INT(10) UNSIGNED NOT NULL,
	orasosire	    DATETIME NOT NULL,
	oraplecare	    DATETIME NOT NULL,
	FOREIGN KEY (idutilizator) REFERENCES utilizatori(idutilizator) ON UPDATE CASCADE ON DELETE CASCADE
);
ALTER TABLE activitatezilnica ADD CONSTRAINT zi_chk CHECK (ziuasaptamanii IN ('luni', 'marti', 'miercuri', 'joi', 'vineri', 'sambata', 'duminica'));

CREATE TABLE salarii (
	idsalariu	    INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	idutilizator	INT(10) UNSIGNED NOT NULL,
	luna		    VARCHAR(10) NOT NULL,
	valoaresalariu	INT(10) NOT NULL,
	FOREIGN KEY (idutilizator) REFERENCES utilizatori(idutilizator) ON UPDATE CASCADE ON DELETE CASCADE
);
ALTER TABLE salarii ADD CONSTRAINT luna_chk CHECK (luna IN ('ian', 'feb', 'mar', 'apr', 'mai', 'iun', 'iul', 'aug', 'sep', 'oct', 'nov', 'dec'));

CREATE TABLE proiecte (
	idproiect	    INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	denumire	    VARCHAR(30) NOT NULL,
	descriere	    VARCHAR(100) NOT NULL
);

CREATE TABLE versiuniproiecte (
	idversiune	    INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	idproiect	    INT(10) UNSIGNED NOT NULL,
	datastart	    DATE NOT NULL,
	dataterminare	DATE NOT NULL,
	denumire	    VARCHAR(10) NOT NULL,
	FOREIGN KEY (idproiect) REFERENCES proiecte(idproiect) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE echipe (
	idechipa	    INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	idproiect	    INT(10) UNSIGNED NOT NULL,
	idresponsabil	INT(10) UNSIGNED NOT NULL,
	FOREIGN KEY (idproiect) REFERENCES proiecte(idproiect) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (idresponsabil) REFERENCES utilizatori(idutilizator) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE asociereechipaangajat (
	idasociere	    INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	idechipa	    INT(10) UNSIGNED NOT NULL,
	idutilizator	INT(10) UNSIGNED NOT NULL,
	datastart	    DATE NOT NULL,
	dataincheiere	DATE NOT NULL,
	FOREIGN KEY (idechipa) REFERENCES echipe(idechipa) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (idutilizator) REFERENCES utilizatori(idutilizator) ON UPDATE CASCADE ON DELETE CASCADE
);

#CREATE TABLE asociereproiectechipa (
#	idasociere	    INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
#	idproiect	INT(10) UNSIGNED NOT NULL,
#	idechipa	    INT(10) UNSIGNED NOT NULL,
#	FOREIGN KEY (idechipa) REFERENCES echipe(idechipa) ON UPDATE CASCADE ON DELETE CASCADE,
#	FOREIGN KEY (idproiect) REFERENCES proiecte(idproiect) ON UPDATE CASCADE ON DELETE CASCADE
#);
#ALTER TABLE asociereproiectechipa ADD CONSTRAINT echipa_exists UNIQUE (idechipa);

CREATE TABLE facturi (
	idfactura	    INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	nrfactura	    INT(10) UNSIGNED NOT NULL,
	idproiect	    INT(10) UNSIGNED NOT NULL,
	totalfactura	DECIMAL(8, 2) UNSIGNED NOT NULL,
	FOREIGN KEY (idproiect) REFERENCES proiecte(idproiect) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE defecte (
	iddefect		        INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	denumire		        VARCHAR(20) NOT NULL,
	severitate		        VARCHAR(20) NOT NULL,
	descriere		        VARCHAR(100) NOT NULL,
	idproiect		        INT(10) UNSIGNED NOT NULL,
	idversiune		        INT(10) UNSIGNED NOT NULL,
	reproductibilitate      VARCHAR(100) NOT NULL,
	statut			        VARCHAR(20) NOT NULL,
	rezultat		        VARCHAR(20) NOT NULL,
	ultimamodificare     	DATETIME NOT NULL,
	idutilizatormodificare	INT(10) UNSIGNED NOT NULL,
	FOREIGN KEY (idproiect) REFERENCES proiecte(idproiect) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (idversiune) REFERENCES versiuniproiecte(idversiune) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (idutilizatormodificare) REFERENCES utilizatori(idutilizator) ON UPDATE CASCADE ON DELETE CASCADE
);
ALTER TABLE defecte ADD CONSTRAINT severitate_chk CHECK (severitate IN ('nu poate fi testat', 'blocarea aplicatiei', 'cerinta esentiala', 'major', 'mediu', 'minor', 'intrebare', 'sugestie'));
ALTER TABLE defecte ADD CONSTRAINT statut_chk CHECK (statut IN ('neanalizat', 'nu poate fi reprodus', 'nu este defect', 'nu va fi corectat'
, 'nu poate fi corectat', 'corectat', 'trebuie corectat'));
ALTER TABLE defecte ADD CONSTRAINT rezultat_chk CHECK (rezultat IN ('defect nou', 'defect verificat', 'defect necorectat'));

CREATE TABLE comentariidefecte (
	idcomentariu	INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	iddefect	    INT(10) UNSIGNED NOT NULL,
	comentariu	    VARCHAR(150) NOT NULL,
	data		    DATETIME NOT NULL,
	FOREIGN KEY (iddefect) REFERENCES defecte(iddefect) ON UPDATE CASCADE ON DELETE CASCADE
);

/* procedura care scade numarul de zile de concediu ale unui angajat din numarul total de zile de coce.. */
DELIMITER //
CREATE PROCEDURE updateZileConcediu(IN userid INT, IN nrzile INT)
BEGIN
UPDATE utilizatori 
SET zileconcediuramase = zileconcediuramase - nrzile
WHERE idutilizator = userid;
END; //
DELIMITER ;

CREATE TABLE debug(
    zi INT,
    data DATE
);
/* functie care intoarce cate zile de concediu mai are un angajat */
DELIMITER //
CREATE FUNCTION zileConcediuRamase(userid INT)
    RETURNS INT
BEGIN
DECLARE zileConcediu INT;
select zileconcediuramase into zileConcediu
from utilizatori
where
    idutilizator = userid;
RETURN zileConcediu;
END; //
DELIMITER ;

/* trigger care se ocupa de updatarea tabele utilizatori la adaugarea unui concediu de odihna */
DELIMITER //
CREATE TRIGGER propaga_concediu_utilizatori BEFORE INSERT ON concedii
FOR EACH ROW
BEGIN
    DECLARE zileRamase INT;
    IF NEW.tip = 'odihna' THEN
        IF zileConcediuRamase(New.idutilizator) >= New.durata THEN
            CALL updateZileConcediu(New.idutilizator, New.durata);
	ELSEIF zileConcediuRamase(New.idutilizator) < New.durata THEN
	    SET NEW = "Error: utilizatorul nu are destule zile de concediu ramase";
        END IF;
    END IF;
END;//
DELIMITER ;

/* functie care intoarce cate zile lucratoare sunt intr-o luna */
DELIMITER //
CREATE FUNCTION zileLucratoare(data DATE)
    RETURNS INT
BEGIN
DECLARE totalZile INT;
DECLARE a INT Default 0;
DECLARE zileLucratoare INT;
DECLARE dayOfWeekNumber INT;
DECLARE startDate DATE;
DECLARE dateDay INT;
DECLARE newDate DATE;
select (DAYOFMONTH(data) - 1) INTO dateDay;
SELECT (data - INTERVAL dateDay DAY) INTO startDate;
SELECT DAY(LAST_DAY(data)) INTO totalZile;
SET zileLucratoare = 0;
simple_loop: LOOP
    select (startDate + INTERVAL a DAY) INTO newDate;
	select DAYOFWEEK(newDate) INTO dayOfWeekNumber;
	IF (dayOfWeekNumber != 1 AND dayOfWeekNumber != 7) THEN
	    set zileLucratoare = zileLucratoare + 1;
	END IF;
    insert INTO debug VALUES(dayOfWeekNumber, newDate);
	SET a = a + 1;
	IF (a = totalZile) THEN
	    LEAVE simple_loop;
	END IF;	
END LOOP simple_loop;
RETURN zileLucratoare;
END; //
DELIMITER ;

/* o functie care intoarce numarul de zile lucratoare dintr-un interval */
DELIMITER //
CREATE FUNCTION zileLucratoareInterval(data1 DATE, data2 Date)
    RETURNS INT
BEGIN
DECLARE totalZile INT;
DECLARE a INT Default 0;
DECLARE zileLucratoare INT;
DECLARE dayOfWeekNumber INT;
DECLARE startDate DATE;
DECLARE dateDay INT;
DECLARE newDate DATE;
SET zileLucratoare = 0;
simple_loop: LOOP
    select (data1 + INTERVAL a DAY) INTO newDate;
	select DAYOFWEEK(newDate) INTO dayOfWeekNumber;
	IF (dayOfWeekNumber != 1 AND dayOfWeekNumber != 7) THEN
	    set zileLucratoare = zileLucratoare + 1;
	END IF;
    insert INTO debug VALUES(dayOfWeekNumber, newDate);
	SET a = a + 1;
    select (data1 + INTERVAL 1 DAY) INTO data1;
	IF (data1 > data2) THEN
	    LEAVE simple_loop;
	END IF;	
END LOOP simple_loop;
RETURN zileLucratoare;
END; //
DELIMITER ;

/* functie care intoarce un numar in functie de severitate */
DELIMITER //
CREATE FUNCTION severitateValue(severitate VARCHAR(30))
    RETURNS INT
BEGIN
DECLARE valoare INT;
case severitate
	WHEN 'aplicatia nu poate fi testata' THEN set valoare = 1;
	WHEN 'blocarea aplicatiei' THEN set valoare = 2;
	WHEN 'cerinta esentiala' THEN set valoare = 3;
	WHEN 'major' THEN set valoare = 4;
	WHEN 'mediu' THEN set valoare = 5;
	WHEN 'minor' THEN set valoare = 6;
	WHEN 'intrebare' THEN set valoare = 7;
	WHEN 'sugestie' THEN set valoare = 8;
	ELSE
		BEGIN
		END;
END CASE;
RETURN valoare;
END; //
DELIMITER ;
